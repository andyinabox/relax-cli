relax-cli
=========

A command line companion for @andyinabox/relax

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/relax-cli.svg)](https://npmjs.org/package/relax-cli)
[![Downloads/week](https://img.shields.io/npm/dw/relax-cli.svg)](https://npmjs.org/package/relax-cli)
[![License](https://img.shields.io/npm/l/relax-cli.svg)](https://github.com/andyinabox/relax-cli/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g relax-cli
$ relax COMMAND
running command...
$ relax (-v|--version|version)
relax-cli/0.0.0 linux-x64 node-v12.13.1
$ relax --help [COMMAND]
USAGE
  $ relax COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`relax hello`](#relax-hello)
* [`relax help [COMMAND]`](#relax-help-command)

## `relax hello`

Describe the command here

```
USAGE
  $ relax hello

OPTIONS
  -n, --name=name  name to print

DESCRIPTION
  ...
  Extra documentation goes here
```

_See code: [src/commands/hello.js](https://github.com/andyinabox/relax-cli/blob/v0.0.0/src/commands/hello.js)_

## `relax help [COMMAND]`

display help for relax

```
USAGE
  $ relax help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.2.0/src/commands/help.ts)_
<!-- commandsstop -->
